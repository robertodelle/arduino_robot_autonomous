   /*
    * Arduino Robot Autonomous with one ultrasonic Sensor HC-SR04
    *
    * by Delle Monache Roberto
    * 
    *
    */
    // defines pins numbers
    const int trigPin = 9;
    const int echoPin = 6;
    
    // defines variables
    long duration;
    int distance;
    int dx_register;
    
    // engines powered by 7 volts step up down converter
    int speedPinL = 7; // PIN PWM engine 1
    int speedPinR = 8; // PIN PWM engine 2

    void setup() {
    pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
    pinMode(echoPin, INPUT); // Sets the echoPin as an Input
    digitalWrite(2, 0);  // first state
    pinMode(2,OUTPUT);   // right engine
    digitalWrite(3, 0);  // first state
    pinMode(3,OUTPUT);   // right engine
    digitalWrite(4, 0);  // first state
    pinMode(4,OUTPUT);   // left engine
    digitalWrite(5, 0);  // first state
    pinMode(5,OUTPUT);   // left engine
    pinMode(speedPinL, OUTPUT);
    pinMode(speedPinR, OUTPUT);
    }

    void loop() {
    // Clears the trigPin
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    distance= duration*0.034/2;
  
    if (distance > 15) { 
        // go forward
        analogWrite(speedPinL, 185);
        analogWrite(speedPinR, 200);
        digitalWrite(2,1); 
        digitalWrite(3,0);
        digitalWrite(4,1);
        digitalWrite(5,0);
    }
    else {
        digitalWrite(2,0); 
        digitalWrite(3,0);
        digitalWrite(4,0);
        digitalWrite(5,0);
  
    }
     }
